#include <iostream>
#include <fstream>
#include <math.h>
#include <string>
#include <map>
#include <cassert>
#include <cstring>
#include <iostream>
#include <queue>
#include <unordered_map>
#include <bitset>
#include <sstream>
#include <limits.h>

using namespace std;

char *chartobin(unsigned char c) {
    static char bin[CHAR_BIT + 1] = {0};
    int i;

    for (i = CHAR_BIT - 1; i >= 0; i--) {
        bin[i] = (c % 2) + '0';
        c /= 2;
    }

    return bin;
}

char stringToChar(string txt) {
    char c1 = static_cast<char>(std::bitset<8>(txt).to_ulong() + 256);
    return c1;
}

int sizeOfFile(FILE *file) {
    FILE *ptr = file;

    fseek(ptr, 0, SEEK_END);

    int length = ftell(ptr) - 1;

    fseek(ptr, 0, SEEK_SET);

    return length;
}

void append(char *s, char c) {
    int len = strlen(s);
    s[len] = c;
    s[len + 1] = '\0';
}

// A Tree node
struct Node {
    char ch;
    int freq;
    Node *left, *right;

};

struct things {
    Node *n;
    string str;
    unordered_map<unsigned char, unsigned int> freq;
};

// Function to allocate a new tree node
Node *getNode(char ch, int freq, Node *left, Node *right) {
    Node *node = new Node();

    node->ch = ch;
    node->freq = freq;
    node->left = left;
    node->right = right;

    return node;
}

// Comparison object to be used to order the heap
struct comp {
    bool operator()(Node *l, Node *r) {
        // highest priority item has lowest frequency
        return l->freq > r->freq;
    }
};

// traverse the Huffman Tree and store Huffman Codes
// in a map.
void encode(Node *root, string str,
            unordered_map<char, string> &huffmanCode) {
    if (root == nullptr)
        return;

    // found a leaf node
    if (!root->left && !root->right) {
        huffmanCode[root->ch] = str;
    }

    encode(root->left, str + "0", huffmanCode);
    encode(root->right, str + "1", huffmanCode);
}

// traverse the Huffman Tree and decode the encoded string
void decode(Node *root, int &index, string str) {
    if (root == nullptr)
        return;

    // found a leaf node
    if (!root->left && !root->right) {
        cout << root->ch;
        return;
    }

    index++;

    if (str[index] == '0')
        decode(root->left, index, str);
    else
        decode(root->right, index, str);
}

void decodaMeuPiru(things t) {
    int index = -1;
//    cout << "\nDecoded string is: \n";
    while (index < (int) t.str.size() - 2) {
        decode(t.n, index, t.str);
    }
}

Node *createTree(const unordered_map<unsigned char, unsigned int> freq) {
    priority_queue<Node *, vector<Node *>, comp> pq;

    unordered_map<unsigned char, unsigned int> freque = freq;
    for (int i = 0; i < 256; i++) {
        pq.push(getNode(i, freque[i], nullptr, nullptr));
    }

    while (pq.size() != 1) {
        Node *left = pq.top();
        pq.pop();
        Node *right = pq.top();
        pq.pop();

        int sum = left->freq + right->freq;
        pq.push(getNode('\0', sum, left, right));
    }

    Node *root = pq.top();

    unordered_map<char, string> huffmanCode;
    encode(root, "", huffmanCode);

    return root;

}

// Builds Huffman Tree and decode given input text
things createTreeAndEncode(string text) {
    things t;
    // count frequency of appearance of each character
    // and store it in a map
    unordered_map<unsigned char, unsigned int> freq;
    for (char ch: text) {
        freq[ch]++;
    }

    t.freq = freq;
    // Create a priority queue to store live nodes of
    // Huffman tree;

    Node *root = createTree(freq);
    t.n = root;

    // traverse the Huffman Tree and store Huffman Codes
    // in a map. Also prints them
    unordered_map<char, string> huffmanCode;
    encode(root, "", huffmanCode);

    string str = "";
    for (char ch: text)
        str += huffmanCode[ch];

    t.str = str;
    return t;
}

things writeFile(char fileLoc[] = "/home/impedro29/Desktop/test.txt") {
    FILE *ptr = fopen(fileLoc, "rb");  // r for read, b for binary
    int lengthOfFile = sizeOfFile(ptr);

    int locationStart = 0;
    int pk = 1;

    fseek(ptr, 0, SEEK_END);
    fseek(ptr, locationStart, SEEK_SET);

    unsigned char buffer[pk];
    char fullString[lengthOfFile];

    for (int i = 0; i < lengthOfFile; i += pk) {
        int ler = (i + pk) > lengthOfFile ? lengthOfFile - i : pk;
        fread(buffer, ler, 1, ptr);
        for (int x = 0; x < ler; x++)
            append(fullString, buffer[x]);
    }

    fclose(ptr);

    things t = createTreeAndEncode(fullString);
    string SfileLoc2;

    stringstream text;

//    cout << treeData;

//    readBinaryTree(n, text);

    int sizeString = 0;
    for (int i = 0; i < strlen(fileLoc); i++) {
        if (fileLoc[i] == 46) {
            sizeString = i;
            break;
        } else SfileLoc2 += fileLoc[i];
    }

    SfileLoc2 += ".phpzin";

    char fileLoc2[sizeString];
    strcpy(fileLoc2, SfileLoc2.c_str());

    FILE *ptrWrite = fopen(fileLoc2, "wb");  // r for read, b for binary

    unsigned byteList[256];
    for (int i = 0; i < 256; i++) {
        byteList[i] = t.freq[i];
    }

    unsigned k[1];
    k[0] = t.str.length() % 8;

//    cout << k[0] << endl;

    fwrite(byteList, 256, sizeof(byteList[0]), ptrWrite);

    fwrite(k, 1, sizeof(k[0]), ptrWrite);

//    cout << t.str.substr(0, 8) << endl;

    for (int i = 8; i + 8 < t.str.length() - k[0]; i += 8) {
        char c = stringToChar(t.str.substr(i - 8, 8));
//        cout << t.str.substr(i - 8, 8) << endl;
        fwrite(&c, 1, sizeof(char), ptrWrite);
    }

    ostringstream str;

//    cout << t.str.substr(t.str.length() - k[0], k[0]) << endl;

    for (int i = 0; i < 8 - k[0]; i++) str << "0";

    str << t.str.substr(t.str.length() - k[0], k[0]);

    cout << str.str() << endl;

    char cha = stringToChar(str.str());

    if (k[0] > 0) fwrite(&cha, 1, sizeof(unsigned), ptrWrite);

    fclose(ptrWrite);

    return t;
}

things readFile(char fileLoc[] = "/home/impedro29/Desktop/test.phpzin") {
    FILE *ptr = fopen(fileLoc, "rb");

    unsigned byteList[256];

    things t;

    unordered_map<unsigned char, unsigned int> bytes;

    fread(byteList, 256, sizeof(byteList[0]), ptr);

    for (int i = 0; i < 256; i++) {
        bytes[i] = byteList[i];
    }

    t.n = createTree(bytes);

    unsigned k[1];

    fread(k, 1, sizeof(k[0]), ptr);

    cout << k[0] << endl;

    unsigned char chars[1]; //essa

//    fread(chars, 1, sizeof(chars[0]), ptr);

//    cout << chartobin(chars[0]) << endl;

    while (fread(chars, 1, sizeof(chars[0]), ptr) && !feof(ptr)) {
        string pa = chartobin(chars[0]);
        if (pa.compare("00000000") != 0) {
            t.str += pa;
            cout << chartobin(chars[0]) << endl;
        }
    }

    t.str = t.str.substr(0, t.str.length());

    if (k[0] != 0) {
        string final = t.str.substr(t.str.length() - 8, 8);
        final = final.substr(k[0], 8 - k[0]);
        cout << final << endl;
        t.str = t.str.substr(0, t.str.length() - 8) + final;
    }

    //asasausihuiduiduh
    //asasausihuiduiduh

    fclose(ptr);

    return t;
}


// Huffman coding algorithm
int main() {

    things t = writeFile();
//    unsigned int freq[256];
//    for(int i =0 ; i < 256 ; i++){
//        freq[i] = t.freq[i];
//    }
//
//    unordered_map<unsigned char, unsigned int> freq2;
//    for (int i = 0 ; i< 256; i++) {
//        cout << i << ": " << t.freq[i] << endl;
//    }



//    for(int i = 0 ; i < 256 ; i++)
//        cout << i << ": " << t.freq[i] << endl;

    things ta = readFile();
    decodaMeuPiru(ta);


//    unsigned *freq = readFile();

//    cout << endl << stringToChar(fullString.substr(0, 8)) << endl;
    return 0;
}
